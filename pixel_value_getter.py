import os
from ij import IJ
from ij.plugin.frame import RoiManager

def load_folder_as_stack(folder):
    '''Ensure that the folder ONLY contains images with proper stack number as file name'''
    file_list = os.listdir(folder)
    IJ.run('Image Sequence...', 'open=' + os.path.join(folder, file_list[0]) + ' sort')

data_directory = IJ.getDirectory('Select data directory')

roi_manager_instance = RoiManager.getRoiManager()
roi_manager_instance.runCommand('Reset')

green_channel_directory = os.path.join(data_directory, 'green')
load_folder_as_stack(green_channel_directory)
IJ.runMacro('rename("GREEN")')
IJ.run('16-bit')

red_channel_directory = os.path.join(data_directory, 'red')
load_folder_as_stack(red_channel_directory)
IJ.runMacro('rename("RED")')
IJ.run('16-bit')

IJ.runMacro('waitForUser("Hit OK when finished with all slices!")')

IJ.runMacro('selectWindow("RED")')
IJ.runMacro("close()")
IJ.runMacro('selectWindow("GREEN")')

pixel_value_list = []
for i in range(roi_manager_instance.getCount()):
    roi_manager_instance.select(i)
    roi = roi_manager_instance.getSelectedRoisAsArray()[0]
    IJ.setSlice(roi_manager_instance.getSliceNumber(roi_manager_instance.getName(i)))
    roi_bounding_rectangle = roi.getBounds()
    roi_y0 = roi_bounding_rectangle.y
    roi_y1 = roi_bounding_rectangle.y + roi_bounding_rectangle.height
    roi_x0 = roi_bounding_rectangle.x
    roi_x1 = roi_bounding_rectangle.x + roi_bounding_rectangle.width
    for x, y in [(x, y) for y in range(roi_y0, roi_y1) for x in range(roi_x0, roi_x1)]:
        if roi.contains(x, y):
            pixel_value_list.append(IJ.getImage().getPixel(x, y)[0])

output_file_path = os.path.join(data_directory, IJ.getString('Enter output filename', '') + '.csv')
output_file = open(output_file_path, 'w')
output_file.write('\n'.join([str(i) for i in pixel_value_list]))
output_file.close()

IJ.runMacro("close()")
